//
//  WeekDay.swift
//  Pedometer
//
//  Created by Кирилл on 18.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

class WeekDay {
    static func activeDay () -> Int{
        let date = Date()
        let calendar = Calendar.current
        let weekday = calendar.component(.weekday, from: date)
        return weekday - 1
    }
}
