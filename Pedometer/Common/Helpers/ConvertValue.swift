//
//  ConvertStepOnKM.swift
//  Pedometer
//
//  Created by Кирилл on 18.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

class ConvertValue {
    static func ConvertStepOnKM(step:Int) -> Double{
//        Формула расчета количества шагов:
//        N = 100000 * R / S, где
//
//        N - количество шагов;
//        R - пройденный путь километрах (км);
//        S - средняя длина одного шага в см.
//
//        Формула расчета средней длины шага:
//        S = L / 4 + 37, где
//        S - длина шага в см;
//        L - рост в см.
        let heigh = UserDefaults.standard.string(forKey: userDefaultsKey.heigh.rawValue) ?? ""
        let L = Double(heigh)!
        let S:Double = L / 4 + 37
        let N = step
        let R:Double = S * Double(N) / 100000
        return R
    }
    
    static func ConvertStepOnСalories(step:Int) -> Double{
        //Q = k * m * l, где
        //
        //Q - количество теплоты в килокалориях;
        //k - коэффициент скорости;
        //        m - масса тела в килограммах;
        //        l - пройденный путь в километрах.
        let k = 1.15
        let m = Double(UserDefaults.standard.string(forKey: userDefaultsKey.width.rawValue)!)
        let l = ConvertStepOnKM(step: step)
        return k * m! * l
    }
}
