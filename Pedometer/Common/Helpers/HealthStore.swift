//
//  HealthStore.swift
//  Pedometer
//
//  Created by Кирилл on 17.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation
import HealthKit

class HealthStore {
    var healthStore: HKHealthStore?
    var query:HKStatisticsCollectionQuery?
    
    init() {
        if HKHealthStore.isHealthDataAvailable(){
            healthStore = HKHealthStore()
        }
    }
    
    func calcularSteps(days:Int = -6,complition: @escaping(HKStatisticsCollection?) -> Void){
        //appleExerciseTime - время активности
        let stepType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
        
        let startDate = Calendar.current.date(byAdding: .day, value: days, to:Date())
        let ancorDate = Date.mondayAt12AM()
        let daily = DateComponents(day: 1)
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: Date(), options: .strictStartDate)
        
        query =  HKStatisticsCollectionQuery(quantityType: stepType, quantitySamplePredicate: predicate, options: .cumulativeSum, anchorDate: ancorDate, intervalComponents: daily)
        query?.initialResultsHandler = { query, statisticsCollectionQuery, error in
            complition(statisticsCollectionQuery)
        }
        
        if let healthStore = healthStore,let query = self.query{
            healthStore.execute(query)
        }
    }
    
    func requestAuthorization(complition: @escaping (Bool) -> Void){
        let stepType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
        
        guard let healthStore = self.healthStore else { return complition(false) }
        healthStore.requestAuthorization(toShare: [], read: [stepType]) { (sessues, error) in
            complition(sessues)
        }
    }
}
