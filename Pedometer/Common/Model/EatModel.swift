//
//  EatModel.swift
//  Pedometer
//
//  Created by Кирилл on 24.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

struct EatModel: Hashable {
    var eatName:String
    var eatCalories:Int = 100
}
