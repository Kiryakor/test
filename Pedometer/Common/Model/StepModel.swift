//
//  StepModel.swift
//  Pedometer
//
//  Created by Кирилл on 17.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation
import SwiftUI

struct Step: Identifiable {
    var id = UUID()
    var count:Int
    var date:Date
    var week:Int
    var color:Color
}
