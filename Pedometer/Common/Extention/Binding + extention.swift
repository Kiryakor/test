//
//  Binding + extention.swift
//  Pedometer
//
//  Created by Кирилл on 18.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation
import SwiftUI

extension Binding {

    func onUpdate(_ closure: @escaping () -> Void) -> Binding<Value> {
        Binding(get: {
            self.wrappedValue
        }, set: { newValue in
            self.wrappedValue = newValue
            closure()
        })
    }
}
