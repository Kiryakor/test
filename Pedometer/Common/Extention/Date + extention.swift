//
//  Date + extention.swift
//  Pedometer
//
//  Created by Кирилл on 17.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

extension Date{
    static func mondayAt12AM() -> Date{
        return Calendar(identifier: .iso8601).date(from: Calendar(identifier: .iso8601)
            .dateComponents([.yearForWeekOfYear,.weekOfYear], from: Date()))!
    }
}
