//
//  CustomNavigationView.swift
//  Pedometer
//
//  Created by Кирилл on 15.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct CustomTapBarView: View {

    @EnvironmentObject var openView:NavigationViewModel
    
    var body: some View {
        HStack{
            Spacer()
            VStack(spacing:0){
                Image("man")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 30, height: 30)
                    .clipShape(Circle())
                LigthGrayTextView(text: "Тренировки")
            }
                .offset(y: 2)
                .onTapGesture {
                    self.openView.presentedView = .isTrainingView
                }
            Spacer()
            CircleImageView(imageName: "man")
                .frame(width: 70, height: 70)
                .offset(y: -30)
                .onTapGesture {
                    self.openView.presentedView = .isPresentMainView
                }
            Spacer()
            VStack(spacing:0){
                Image("man")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 30, height: 30)
                    .clipShape(Circle())
                Text("Питание")
                    .font(.footnote)
                    .foregroundColor(.gray)
            }
                .offset(y: 2)
                .onTapGesture {
                    self.openView.presentedView = .isEatView
                }
            Spacer()
        }
        .frame(width: UIScreen.main.bounds.width, height: 49)
        .background(shapeTapBar())
    }
}

struct CustomNavigationView_Previews: PreviewProvider {
    
    static var previews: some View {
        CustomTapBarView().environmentObject(NavigationViewModel())
    }
}
