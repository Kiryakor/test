//
//  BigBlueButtonView].swift
//  Pedometer
//
//  Created by Кирилл on 15.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct BigBlueButtonView: View {
    
    var text:String
    var action: (() -> Void)?
    
    var body: some View {
        VStack(spacing:0){
            if action != nil{
                Button(action: action!) {
                    TextSubBigBlueButtonView(text: text)
                }
                .padding(.bottom,16)
            }else{
                Button(action: {
                    
                }) {
                    TextSubBigBlueButtonView(text: text)
                }
                .padding(.bottom,16)
            }
        }
    }
    
    func foo(perform action: @escaping () -> Void ) -> Self {
        var copy = self
        copy.action = action
        return copy
    }
    
    struct TextSubBigBlueButtonView: View {
        
        var text:String
        
        var body: some View {
            Text(text)
                .foregroundColor(.white)
                .font(.headline)
                .frame(width: UIScreen.main.bounds.width - 64, height: 50)
                .background(Color.blue)
                .cornerRadius(25)
                .shadow(radius: 5, y: 2)
        }
    }

}

struct BigBlueButtonView__Previews: PreviewProvider {
    
    static var previews: some View {
        BigBlueButtonView(text: "Кнопка").previewLayout(.fixed(width: UIScreen.main.bounds.width, height: 150))
    }
}
