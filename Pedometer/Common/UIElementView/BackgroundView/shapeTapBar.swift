//
//  shapeTapBar.swift
//  Pedometer
//
//  Created by Кирилл on 16.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct shapeTapBar: View {
    var body: some View {
        Path { (path) in
            path.move(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: UIScreen.main.bounds.width, y: 0))
            path.addLine(to: CGPoint(x: UIScreen.main.bounds.width, y: 49))
            path.addArc(center: CGPoint(x: (UIScreen.main.bounds.width / 2) - 11, y: 49), radius: 49, startAngle: .zero, endAngle: .degrees(180), clockwise: false)
            path.addLine(to: CGPoint(x: 0, y: 49))
        }
        .fill(Color.init(red: 220/255, green: 220/255, blue: 220/255))
        .rotationEffect(.init(degrees: 180))
    }
}


struct shapeTapBar_Previews: PreviewProvider {
    static var previews: some View {
        shapeTapBar()
    }
}
