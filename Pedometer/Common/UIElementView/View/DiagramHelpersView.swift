//
//  DiagramHelpersView.swift
//  Pedometer
//
//  Created by Кирилл on 18.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct DiagramHelpersView:View {
    
    var step:[Step]
    var maxCount:Int
    var weekDay:[String] = ["1","5","10","15","20","25","30"]
    
    var body: some View{
        VStack{
            if step.count < 8{
                HStack{
                    ForEach(0..<self.step.count, id: \.self) { (index) in
                        DiagramView(step: self.step[index], maxCount: self.maxCount)
                    }
                }
            }else if step.count < 32{
                HStack(spacing: 2){
                    ForEach(0..<self.step.count, id: \.self) { (index) in
                        DiagramMonthView(step: self.step[index], maxCount: self.maxCount)
                    }
                }
                HStack{
                    ForEach(weekDay, id: \.self) { (element)  in
                        HStack{
                            LigthGrayTextView(text: element)
                            if element != self.weekDay.last{
                                Spacer()
                            }
                        }
                    }
                }
            }else{
                Text("")
            }
        }
    }
}
