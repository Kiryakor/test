//
//  DiagramMonthView.swift
//  Pedometer
//
//  Created by Кирилл on 18.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct DiagramMonthView: View {
    
    var step:Step
    var maxCount:Int
    @State var procent:Double = 0
    
    var body: some View {
        VStack(){
            HStack{
                ZStack{
                    GeometryReader { (geo)  in
                        Rectangle()
                            .foregroundColor(Color.init(red: 220/255, green: 220/255, blue: 220/255))
                        VStack{
                            Spacer(minLength: 0)
                            Rectangle()
                                .frame(width: geo.size.width, height: ((geo.size.height - 40) * CGFloat(self.procent)))
                                .foregroundColor(self.step.color)
                        }
                    }
                }
            }
        }.onAppear{
            self.stepOnProcent()
        }
    }
    
    func stepOnProcent() {
        if step.count > maxCount{
            procent = 1
        }else{
            procent = Double(step.count) / Double(maxCount)
        }
    }
}
