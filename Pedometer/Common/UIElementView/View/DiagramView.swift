//
//  DiagramView.swift
//  Pedometer
//
//  Created by Кирилл on 17.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct DiagramView: View {
    
    var weekDay:[String] = ["Пн","Вт","Ср","Чт","Пт","Сб","Вс"]
    var step:Step
    var maxCount:Int
    @State var procent:Double = 0
    
    var body: some View {
        VStack(spacing:0){
            ZStack{
                GeometryReader { (geo)  in
                    Rectangle()
                        .foregroundColor(Color.init(red: 220/255, green: 220/255, blue: 220/255))
                    VStack(){
                        Spacer(minLength: 0)
                        Text("\(self.step.count)")
                            .font(.system(size: 10))
                        Rectangle()
                            .frame(width: geo.size.width, height: ((geo.size.height - 40) * CGFloat(self.procent)))
                            .foregroundColor(self.step.color)
                    }
                }
            }
            LigthGrayTextView(text: weekDay[step.week])
        }.onAppear{
            self.stepOnProcent()
        }
    }
    
    func stepOnProcent() {
        if step.count > maxCount{
            procent = 1
        }else{
            procent = Double(step.count) / Double(maxCount)
        }
    }
}

struct DiagramView_Previews: PreviewProvider {
    static var previews: some View {
        DiagramView(step: Step(count: 1234, date: Date(), week: 3, color: .green), maxCount: 10000).previewLayout(.fixed(width: 100, height: 500))
    }
}
