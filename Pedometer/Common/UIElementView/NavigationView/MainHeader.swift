//
//  MainHeader.swift
//  Pedometer
//
//  Created by Кирилл on 16.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct MainHeader: View {
    
    @EnvironmentObject var openView:NavigationViewModel
    
    var body: some View {
        VStack{
            HStack(spacing:16){
                Text("PEDOMENT")
                    .font(.title)
                    .fontWeight(.bold)
                    .padding(.leading)
                Spacer()
            }
            .padding([.trailing,.top],16)
            .frame(width: UIScreen.main.bounds.width, height: 44)
            Divider()
                .padding(.horizontal)
            HStack(spacing:0){
                HStack{
                    BaseImageView(imageName: "man")
                    BaseImageView(imageName: "man")
                    BaseImageView(imageName: "man")
                    BaseImageView(imageName: "man")
                    BaseImageView(imageName: "man")
                }
                .padding(.leading)
                VStack(spacing:0){
                    Text("Награды")
                        .font(.headline)
                    Text("Смотреть все")
                        .foregroundColor(.gray)
                        .font(.system(size: 13))
                }
                .padding(.horizontal)
                .onTapGesture {
                    self.openView.presentedView = .isAchievementsView
                }
            }
            .frame(width: UIScreen.main.bounds.width, height: 44)
        }
        .padding(.bottom,8)
        //.background(Color.init(red: 220/255, green: 220/255, blue: 220/255))
    }
}


struct MainHeader_Previews: PreviewProvider {
    
    static var previews: some View {
        MainHeader().previewLayout(.fixed(width: UIScreen.main.bounds.width, height: 150))
    }
}
