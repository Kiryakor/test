//
//  HeaderView.swift
//  Pedometer
//
//  Created by Кирилл on 26.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct HeaderWithButtonView: View {
    
    let leftImage:String?
    let rightImage:String?
    let leftAction:() -> Void
    let rightAction:() -> Void
    let title:String = "Title"
    
    var body: some View {
        VStack{
            HStack{
                BaseImageView(imageName: leftImage ?? "defuat")
                    .padding(.leading)
                    .onTapGesture(perform: leftAction)
                Spacer()
                Text(title)
                    .font(.title)
                    .fontWeight(.bold)
                    .multilineTextAlignment(.center)
                    .layoutPriority(1)
                Spacer()
                BaseImageView(imageName: rightImage ?? "defuat")
                    .padding(.trailing)
                    .onTapGesture(perform: rightAction)
            }
                .padding(.top,16)
                .frame(width: UIScreen.main.bounds.width, height: 44)
            Divider()
                .padding(.horizontal)
        }
    }
}


struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderWithButtonView(leftImage: "cancel", rightImage: "cancel", leftAction: {}, rightAction: {})
            .previewLayout(.fixed(width: UIScreen.main.bounds.width, height: 100))
    }
}
