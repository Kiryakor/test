//
//  AlertView.swift
//  Pedometer
//
//  Created by Кирилл on 15.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct AlertTextField: View {
    
    @State var text:String = ""
    @Binding var returnValue:String
    @Binding var widthHeight:widthHeightEnum
    @Binding var dismiss:Bool
    
    var body: some View{
        VStack{
            Text("Введите ваш \(widthHeight.rawValue.lowercased())")
                .font(.headline)
            TextField("   \(widthHeight.rawValue)", text: $text)
                .border(Color.black, width: 1)
                .keyboardType(.numberPad)
            Divider()
            HStack(spacing:0){
                Spacer(minLength: 0)
                Button(action: {
                    self.returnValue = self.text
                    self.dismiss.toggle()
                }, label: {
                    Text("Сохранить")
                })
                    .padding()
                Spacer(minLength: 0)
                Divider()
                Spacer(minLength: 0)
                Button(action: {
                    self.dismiss.toggle()
                }, label: {
                    Text("  Отмена ")
                })
                    .padding()
                Spacer(minLength: 0)
            }
        }
        .padding()
        .padding(.top)
        .frame(width: UIScreen.main.bounds.width / 1.2, height: 150)
        .border(Color.black)
    }
}

struct AlertView_Previews: PreviewProvider {
    
    @State static var t:String = "что-то"
    @State static var z:widthHeightEnum = .height
    @State static var dismiss = true
    
    static var previews: some View {
        AlertTextField(returnValue: $t, widthHeight: $z, dismiss: $dismiss).previewLayout(.fixed(width: UIScreen.main.bounds.width / 1.2, height: 150))
    }
}
