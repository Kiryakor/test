//
//  BaseImageView.swift
//  Pedometer
//
//  Created by Кирилл on 15.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct BaseImageView: View {
    
    var imageName:String
    
    var body: some View {
        Image(imageName)
            .resizable()
            .scaledToFit()
    }
}

struct BaseImageView_Previews: PreviewProvider {
    static var previews: some View {
        BaseImageView(imageName: "man").previewLayout(.fixed(width: 200, height: 200))
    }
}
