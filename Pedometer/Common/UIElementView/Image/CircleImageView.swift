//
//  CircleImageView.swift
//  Pedometer
//
//  Created by Кирилл on 15.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct CircleImageView: View {
    
    var imageName:String
    
    var body: some View {
        BaseImageView(imageName: imageName)
            .clipShape(Circle())
    }
}

struct CircleImageView_Previews: PreviewProvider {
    static var previews: some View {
        CircleImageView(imageName: "man").previewLayout(.fixed(width: 200, height: 200))
    }
}
