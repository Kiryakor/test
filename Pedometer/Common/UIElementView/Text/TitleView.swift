//
//  TitleView.swift
//  Pedometer
//
//  Created by Кирилл on 15.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct TitleView: View {
    
    var text:String
    
    var body: some View {
        Text(text)
            .foregroundColor(.blue)
            .font(.title)
            .padding(.top,50)
    }
}

struct TitleView_Previews: PreviewProvider {
    static var previews: some View {
        TitleView(text: "Мой Профайл").previewLayout(.fixed(width: 300, height: 100))
    }
}
