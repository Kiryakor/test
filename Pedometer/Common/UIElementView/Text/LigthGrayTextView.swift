//
//  LigthGrayTextView.swift
//  Pedometer
//
//  Created by Кирилл on 16.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct LigthGrayTextView: View {
    
    var text:String
    
    var body: some View {
        Text(text)
            .font(.footnote)
            .foregroundColor(.gray)
    }
}

struct LigthGrayTextView_Previews: PreviewProvider {
    
    static var text = "Content"
    
    static var previews: some View {
        LigthGrayTextView(text: text).previewLayout(.fixed(width: 100, height: 50))
    }
}
