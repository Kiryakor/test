//
//  SubTitleView.swift
//  Pedometer
//
//  Created by Кирилл on 15.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct SubTitleView: View {
    
    var text:String
    
    var body: some View {
        Text(text)
            .foregroundColor(.gray)
            .font(.system(size: 13))
            .padding(.top,20)
    }
}

struct SubTitleView_Previews: PreviewProvider {
    static var previews: some View {
        SubTitleView(text: "dfgtdhgtsdgsfdf").previewLayout(.fixed(width: 300, height: 100))
    }
}
