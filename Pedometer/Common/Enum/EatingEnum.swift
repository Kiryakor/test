//
//  EatingEnum.swift
//  Pedometer
//
//  Created by Кирилл on 24.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

enum EatingEnum:String{
    case breakfast = "Завтрак"
    case lunch = "Обед"
    case dinner = "Ужин"
    case snack = "Другое"
}
