//
//  NavigationEnum.swift
//  Pedometer
//
//  Created by Кирилл on 16.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

enum NavigationEnum {
    case isSexChoose
    case isMyProfileView
    case isPresentMainView
    case isAchievementsView // достижения
    case isEatView
    case isTrainingView
    case isAddEatView
    case isDetailEatView
}
