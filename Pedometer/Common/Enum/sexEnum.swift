//
//  sexEnum.swift
//  Pedometer
//
//  Created by Кирилл on 24.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

enum sexEnum:String {
    case man = "мужчина"
    case girl = "женщина"
    case noChoose = "не выбран"
}
