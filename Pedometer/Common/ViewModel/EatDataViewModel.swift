//
//  EatData.swift
//  Pedometer
//
//  Created by Кирилл on 24.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

class EatDataViewModel:ObservableObject{
    @Published var breakfast:[EatModel] = []
    @Published var lunch:[EatModel] = []
    @Published var dinner:[EatModel] = []
    @Published var snack:[EatModel] = []
    
    @Published var sumCal:Int = 0
    @Published var dayNorma:Int = 2600
    
    init() {
        sumCal = sum()
    }
}

extension EatDataViewModel{
    func sum() -> Int{
        var sum = 0
        for i in breakfast{
            sum += i.eatCalories
        }
        for i in lunch {
            sum += i.eatCalories
        }
        for i in dinner{
            sum += i.eatCalories
        }
        for i in snack {
            sum += i.eatCalories
        }
        return sum
    }
}
