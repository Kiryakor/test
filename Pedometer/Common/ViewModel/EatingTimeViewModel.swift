//
//  EatingTimeViewModel.swift
//  Pedometer
//
//  Created by Кирилл on 24.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

class EatingTimeViewModel: ObservableObject {
    @Published var eatingTime:EatingEnum = .breakfast
}
