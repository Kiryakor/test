//
//  AllEatViewModel.swift
//  Pedometer
//
//  Created by Кирилл on 24.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

class AllEatViewModel: ObservableObject {
    @Published var allEats:[EatModel] = [EatModel(eatName: "Шашлык"),EatModel(eatName: "Пицца"),EatModel(eatName: "Суши"),EatModel(eatName: "Вок"),EatModel(eatName: "Гречка")]
}
