//
//  SettingView.swift
//  Pedometer
//
//  Created by Кирилл on 16.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct EatView: View {
    
    @EnvironmentObject var openView:NavigationViewModel
    @EnvironmentObject var eatData:EatDataViewModel
    @EnvironmentObject var eatTime:EatingTimeViewModel
    
    var body: some View {
        VStack{
            HeaderWithButtonView(leftImage: "man", rightImage: "man", leftAction: {}, rightAction: {})
            HStack{
                VStack(alignment:.leading,spacing: 8){
                    Text("Осталось Калорий")
                    Text("Употреблено Калорий")
                }
                .padding(.leading,32)
                Spacer()
                VStack(alignment:.trailing,spacing: 8){
                    Text(String(eatData.dayNorma - eatData.sum()))
                        .foregroundColor(.gray)
                    Text(String(eatData.sum()))
                        .bold()
                }
                .padding(.trailing,32)
            }
            .padding(.top,6)
            Form{
                CellView(data: eatData.breakfast, index: 0, titleText: "Завтрак").environmentObject(openView).environmentObject(eatTime)
                CellView(data: eatData.lunch, index: 1, titleText: "Обед").environmentObject(openView).environmentObject(eatTime)
                CellView(data: eatData.dinner, index: 2, titleText: "Ужин").environmentObject(openView).environmentObject(eatTime)
                CellView(data: eatData.snack, index: 3, titleText: "Перекус").environmentObject(openView).environmentObject(eatTime)
            }
            .padding(.bottom,-8)
            CustomTapBarView().environmentObject(openView)
        }.onAppear {
            UITableView.appearance().backgroundColor = .white
            UITableView.appearance().tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: Double.leastNonzeroMagnitude))
            UITableView.appearance().separatorStyle = .none
        }
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct SettingView_Previews: PreviewProvider {
    static var previews: some View {
        EatView()
            .environmentObject(NavigationViewModel())
            .environmentObject(EatDataViewModel())
            .environmentObject(EatingTimeViewModel())
    }
}
