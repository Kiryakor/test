//
//  DetailEatView.swift
//  Pedometer
//
//  Created by Кирилл on 26.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct DetailEatView: View {
    
    @EnvironmentObject var openView:NavigationViewModel
    
    var body: some View {
        VStack{
            HeaderWithButtonView(leftImage: "cancel", rightImage: nil, leftAction: { self.openView.presentedView = .isEatView}, rightAction: {})
            Spacer()
            CustomTapBarView().environmentObject(openView)
        }.edgesIgnoringSafeArea(.bottom)
    }
}

struct DetailEatView_Previews: PreviewProvider {
    static var previews: some View {
        DetailEatView()
            .environmentObject(NavigationViewModel())
    }
}
