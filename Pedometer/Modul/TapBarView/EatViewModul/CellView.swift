//
//  CellView.swift
//  Pedometer
//
//  Created by Кирилл on 24.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct CellView: View {
    
    @State var data:[EatModel] = []
    var index:Int
    var titleText:String
    
    @EnvironmentObject var openView:NavigationViewModel
    @EnvironmentObject var eatTime:EatingTimeViewModel
    
    @State var caloriesSum:Int = 0
    
    var body: some View {
        VStack{
            HStack{
                BaseImageView(imageName: "man")
                    .frame(width: 30, height: 30)
                    .padding(.leading)
                Text(titleText)
                    .font(.title)
                    .layoutPriority(1)
                Spacer()
                VStack(alignment:.trailing){
                    Text(String(caloriesSum))
                    Text("Калории")
                }
                Image(systemName: "plus")
                    .foregroundColor(.green)
                    .frame(width: 30, height: 30)
                    .onTapGesture {
                        if self.index == 0{
                            self.eatTime.eatingTime = .breakfast
                        }else if self.index == 2{
                            self.eatTime.eatingTime = .dinner
                        }else if self.index == 1{
                            self.eatTime.eatingTime = .lunch
                        }else{
                            self.eatTime.eatingTime = .snack
                        }
                        
                        self.openView.presentedView = .isAddEatView
                    }
            }
            .padding(.trailing,16)
            .padding(.top)
            if data.count > 0{
                List{
                    ForEach(data, id: \.self) { (element)  in
                        VStack{
                            Divider()
                            HStack{
                                VStack(alignment:.leading){
                                    Text(element.eatName)
                                    Text("100 g")
                                        .foregroundColor(.green)
                                }
                                .padding(.leading)
                                Color(.white)
                                Text(String(element.eatCalories))
                                    .foregroundColor(.gray)
                                BaseImageView(imageName: "next")
                                    .frame(width: 30, height: 20)
                                    .padding(.trailing)
                            }.onTapGesture {
                                self.openView.presentedView = .isDetailEatView
                            }
                        }
                    }
                }
            }else{
                VStack{
                    Divider()
                    Text("Продукты еще не были добавлены")
                        .font(.system(size: 16))
                        .lineLimit(1)
                        .padding(.vertical,8)
                }
            }
        }
        .padding(.bottom,8)
        .overlay(
            RoundedRectangle(cornerRadius: 5)
                .stroke(Color.gray, lineWidth: 0.5)
        )
        .onAppear{
            self.caloriesSum = self.sumData()
        }
    }
}

extension CellView{
    func sumData() -> Int {
        var sum = 0
        for i in data{
            sum += i.eatCalories
        }
        return sum
    }
}
