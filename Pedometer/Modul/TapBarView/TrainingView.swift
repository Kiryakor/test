//
//  TrainingView.swift
//  Pedometer
//
//  Created by Кирилл on 21.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct TrainingView: View {
    
    @EnvironmentObject var openView:NavigationViewModel
    
    var body: some View {
        VStack{
            Spacer()
            CustomTapBarView().environmentObject(openView)
        }
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct TrainingView_Previews: PreviewProvider {
    
    static var previews: some View {
        TrainingView().environmentObject(NavigationViewModel())
    }
}
