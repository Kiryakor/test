//
//  StatisticsView.swift
//  Pedometer
//
//  Created by Кирилл on 16.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI
import HealthKit

struct MainView: View {
    
    @EnvironmentObject var openView:NavigationViewModel
    @State private var period = 0
    @State var step:[Step] = []
    @State var stepSum:Int = 0
    @State var chooseDayCount = [-6,-29]
    @State var maxStep = 1
    @State var middleStepCount:Int = 0
    @State var kmValue:Double = 0
    @State var middlekmValue:Double = 0
    @State var caloriesCount:Double = 0
    @State var middleCaloriesCount:Double = 0
    private var healthStore:HealthStore?
    
    init() {
        healthStore = HealthStore()
    }
    
    var body: some View {
        VStack(spacing: 0){
            MainHeader().environmentObject(openView)
            VStack{
                Picker(selection: $period.onUpdate(healthAuthorization), label: Text("")) {
                    Text("7 дней").tag(0)
                    Text("30 дней").tag(1)
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding()
                .padding(.horizontal)
                Text("Всего")
                    .fontWeight(.bold)
                    .padding(.bottom,4)
                HStack{
                    Spacer()
                    VStack{
                        LigthGrayTextView(text: "Шаги")
                        Text("\(self.stepSum)")
                            .fontWeight(.bold)
                    }
                    .frame(width: 100)
                    Spacer()
                    VStack{
                        LigthGrayTextView(text: "Км")
                        Text(String(format: "%.2f", self.kmValue))
                            .fontWeight(.bold)
                    }
                    .frame(width: 100)
                    Spacer()
                    VStack{
                        LigthGrayTextView(text: "Ккал")
                        Text(String(format: "%.1f", self.caloriesCount))
                            .fontWeight(.bold)
                    }
                    .frame(width: 100)
                    Spacer()
                }
                Divider().padding(.horizontal,64)
                Text("В среднем за день")
                    .fontWeight(.bold)
                    .padding(.bottom,4)
                HStack{
                    Spacer()
                    VStack{
                        LigthGrayTextView(text: "Шаги")
                        Text("\(self.middleStepCount)")
                            .fontWeight(.bold)
                    }
                    .frame(width: 100)
                    Spacer()
                    VStack{
                        LigthGrayTextView(text: "Км")
                        Text(String(format: "%.2f", self.middlekmValue))
                            .fontWeight(.bold)
                    }
                    .frame(width: 100)
                    Spacer()
                    VStack{
                        LigthGrayTextView(text: "Ккал")
                         Text(String(format: "%.1f", self.middleCaloriesCount))
                            .fontWeight(.bold)
                    }
                    .frame(width: 100)
                    Spacer()
                }
            }
            Spacer()
            HStack{
                DiagramHelpersView(step: self.step, maxCount: self.maxStep)
            }
            .padding(.bottom,55)
            .padding(.horizontal)
            CustomTapBarView().environmentObject(openView)
        }
        .onAppear{
            self.healthAuthorization()
        }
        .edgesIgnoringSafeArea(.bottom)
    }
}

extension MainView{
    private func healthAuthorization(){
        if let healthStore = self.healthStore{
            healthStore.requestAuthorization { (susses) in
                if susses{
                    healthStore.calcularSteps { (statisticCollection) in
                        if let statisticCollection = statisticCollection{
                            self.updateUIFromStatistics(statisticCollection)
                        }
                    }
                }
            }
        }
    }
    
    private func updateUIFromStatistics(_ statisticsCollection: HKStatisticsCollection) {
        var activeDay = WeekDay.activeDay()
        self.step.removeAll()
        let startDate = Calendar.current.date(byAdding: .day, value: self.chooseDayCount[period], to: Date())!
        let endDate = Date()
        statisticsCollection.enumerateStatistics(from: startDate, to: endDate) { (statistics, stop) in
            let count = statistics.sumQuantity()?.doubleValue(for: .count())
            let step = Step(count: Int(count ?? 0), date: statistics.startDate, week: activeDay, color: .green)
            (activeDay == 6) ? (activeDay = 0) : (activeDay += 1)
            self.step.append(step)
        }
        getDataFromStep()
    }
    
    private func getDataFromStep(){
        self.stepSum = 0
        self.kmValue = 0
        self.caloriesCount = 0
        self.step.forEach { (step) in
            self.stepSum += step.count
            self.kmValue += ConvertValue.ConvertStepOnKM(step: step.count)
            self.caloriesCount = ConvertValue.ConvertStepOnСalories(step: step.count)
            if maxStep < step.count{ maxStep = step.count }
        }
        self.middleStepCount = self.stepSum / self.step.count
        self.middlekmValue = self.kmValue / Double(self.step.count)
        self.middleCaloriesCount = self.caloriesCount / Double(self.step.count)
    }
}

struct StatisticsView_Previews: PreviewProvider {
    
    static var previews: some View {
        MainView().environmentObject(NavigationViewModel())
    }
}
