//
//  MyProfileView.swift
//  Pedometer
//
//  Created by Кирилл on 15.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct MyProfileView: View {
    
    @State var widthValue:String = "70"
    @State var heightValue:String = "170"
    @State var widthHeight:widthHeightEnum = .height
    @State var showAlert:Bool = false
    @EnvironmentObject var openView:NavigationViewModel
    let myProfileViewModel = MyProfileViewModel()
    
    var body: some View {
        ZStack{
            VStack{
                VStack {
                    TitleView(text: "Мой Профайл")
                    SubTitleView(text: "Это нужно для расчета длины шага и калорий.")
                }
                VStack{
                    Spacer()
                    Divider()
                        .padding()
                    Spacer()
                }
                VStack(spacing: 20) {
                    Text("Рост")
                        .foregroundColor(.gray)
                    HStack(spacing: 40){
                        Text("\(heightValue) см.")
                            .font(.headline)
                        Text("∇")
                            .foregroundColor(.blue)
                            .onTapGesture {
                                self.showAlert.toggle()
                                self.widthHeight = .height
                        }
                    }
                }
                .padding()
                VStack{
                    Spacer()
                    Divider()
                        .padding()
                    Spacer()
                }
                VStack(spacing: 20) {
                    Text("Вес")
                        .foregroundColor(.gray)
                    HStack(spacing: 40){
                        Text("\(widthValue) кг.")
                            .font(.headline)
                        Text("∇")
                            .foregroundColor(.blue)
                            .onTapGesture {
                                self.showAlert.toggle()
                                self.widthHeight = .width
                        }
                    }
                }
                .padding()
                VStack{
                    Spacer()
                    Divider()
                        .padding()
                    Spacer()
                }
                BigBlueButtonView(text: "Далее").foo {
                    self.openView.presentedView = .isPresentMainView
                }
                    .padding(.bottom,16)
            }
            .opacity(showAlert ? 0.5 : 1)
            .blur(radius: self.showAlert ? 10 : 0)
            if self.showAlert{
                if widthHeight == .height {
                    AlertTextField(returnValue: $heightValue, widthHeight: $widthHeight, dismiss: $showAlert)
                        .shadow(radius: 5,x: 2,y: 2)
                        .padding(.bottom,50)
                }else if widthHeight == .width {
                    AlertTextField(returnValue: $widthValue, widthHeight: $widthHeight, dismiss: $showAlert)
                        .shadow(radius: 5,x: 2,y: 2)
                        .padding(.bottom,50)
                }
            }
        }.onDisappear{
            self.myProfileViewModel.saveHeigh(value: self.heightValue)
            self.myProfileViewModel.saveWidth(value: self.widthValue)
        }
    }
}

struct MyProfileView_Previews: PreviewProvider {
    
    static var previews: some View {
        MyProfileView()
    }
}
