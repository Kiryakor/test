//
//  MyProfileViewModel.swift
//  Pedometer
//
//  Created by Кирилл on 17.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

class MyProfileViewModel {
    func saveWidth(value:String){
        UserDefaults.standard.set(value, forKey: userDefaultsKey.width.rawValue)
    }
    
    func saveHeigh(value:String){
        UserDefaults.standard.set(value, forKey: userDefaultsKey.heigh.rawValue)
    }
}
