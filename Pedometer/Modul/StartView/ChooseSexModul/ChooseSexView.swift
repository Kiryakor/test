//
//  ContentView.swift
//  Pedometer
//
//  Created by Кирилл on 15.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct ChooseSexView: View {
    
    @State var chooseSex:sexEnum = .noChoose
    @State var opacityNextButton: Double = 0
    @EnvironmentObject var openView:NavigationViewModel
    let chooseSexViewModel = ChooseSexViewModel()
    
    var body: some View {
        ZStack{
            VStack{
                Spacer(minLength: 0)
                TitleView(text: "Выберите пол")
                SubTitleView(text: "Это нужно для расчета длины шага и калорий.")
                Spacer(minLength: 0)
                HStack(spacing: 50){
                    VStack{
                        if chooseSex == .girl{
                            CircleImageView(imageName: "girl")
                                .clipShape(Circle())
                                .overlay(Circle().stroke(Color.green, lineWidth: 5))
                        }else{
                            CircleImageView(imageName: "girl")
                            .clipShape(Circle())
                        }
                        Text("Женский")
                            .foregroundColor(.gray)
                    }
                        .onTapGesture {
                            self.chooseSex = .girl
                            withAnimation {
                                self.opacityNextButton = 1
                            }
                    }
                    VStack{
                        if chooseSex == .man{
                            CircleImageView(imageName: "man")
                                .overlay(Circle().stroke(Color.green, lineWidth: 5))
                        }else{
                            CircleImageView(imageName: "man")
                        }
                        Text("Мужской")
                            .foregroundColor(.gray)
                    }
                        .onTapGesture {
                            self.chooseSex = .man
                            withAnimation {
                                self.opacityNextButton = 1
                            }
                        }
                }
                .padding(.horizontal,40)
                Spacer(minLength: 0)
                Spacer(minLength: 0)
            }
            if chooseSex != .noChoose {
                VStack{
                    GeometryReader { (geo) in
                        VStack{
                            Spacer()
                            BigBlueButtonView(text: "Далее").foo {
                                self.openView.presentedView = .isMyProfileView
                                self.chooseSexViewModel.saveSex(value: self.chooseSex.rawValue)
                            }
                                .padding(.bottom,16)
                        }
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        ChooseSexView().environmentObject(NavigationViewModel())
    }
}

