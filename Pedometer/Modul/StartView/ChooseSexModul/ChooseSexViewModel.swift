//
//  ChooseSexViewModel.swift
//  Pedometer
//
//  Created by Кирилл on 17.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

class ChooseSexViewModel {
    func saveSex(value:String){
        UserDefaults.standard.set(value, forKey: userDefaultsKey.sex.rawValue)
    }
}
