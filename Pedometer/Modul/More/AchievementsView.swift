//
//  AchievementsView.swift
//  Pedometer
//
//  Created by Кирилл on 16.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct AchievementsView: View {
    
    @EnvironmentObject var openView:NavigationViewModel
    
    var body: some View {
        VStack(spacing:0){
            HStack{
                Button(action: {
                    self.openView.presentedView = .isPresentMainView
                }, label: {
                    Text("<-")
                })
                    .padding(.leading)
                Text("CЕГОДНЯ")
                    .font(.title)
                    .fontWeight(.bold)
                    .padding(.leading)
                Spacer()
            }
            .frame(width: UIScreen.main.bounds.width, height: 44)
            .padding(.bottom)
            Divider()
                .padding(.horizontal)
            ScrollView(.vertical, showsIndicators: false) {
                VStack{
                    Text("Шаги за день")
                        .font(.headline)
                        .padding(.top)
                    HStack{
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                    }
                    HStack{
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                    }
                    HStack{
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                    }
                }
                Divider().padding(.horizontal)
                VStack{
                    Text("Шаги за день")
                        .font(.headline)
                    HStack{
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                    }
                    HStack{
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                    }
                    HStack{
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                    }
                }
                Divider().padding(.horizontal)
                VStack{
                    Text("Шаги за день")
                        .font(.headline)
                    HStack{
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                    }
                    HStack{
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                    }
                    HStack{
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                        VStack{
                            BaseImageView(imageName: "man")
                            LigthGrayTextView(text: "text")
                        }
                        .padding(.horizontal)
                    }
                }
            }
        }
    }
}

struct AchievementsView_Previews: PreviewProvider {
    
    static var previews: some View {
        AchievementsView()
    }
}
