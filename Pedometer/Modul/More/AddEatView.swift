//
//  AddEatView.swift
//  Pedometer
//
//  Created by Кирилл on 22.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI

struct AddEatView: View {
    
    struct EatChoose {
        var eat:EatModel
        var choose:Bool
    }
    
    @EnvironmentObject var openView:NavigationViewModel
    @EnvironmentObject var eatData:EatDataViewModel
    @EnvironmentObject var eatTime:EatingTimeViewModel
    @ObservedObject var allEat:AllEatViewModel = AllEatViewModel()
    @State private var searchText:String = ""
    @State var chooseEat:[String] = []
    @State var data:[EatModel] = []
    
    var body: some View {
        VStack{
            HeaderWithButtonView(leftImage: "cancel", rightImage: "add", leftAction: {
                self.openView.presentedView = .isEatView
            }, rightAction: {
                self.eatChoose()
                self.openView.presentedView = .isEatView
            })
            
            SearchBar(text: $searchText)
            
            List {
                ForEach(self.allEat.allEats.filter {
                    self.searchText.isEmpty ? true : $0.eatName.contains(self.searchText)
                }, id: \.self) { eat in
                    HStack{
                        HStack{
                            VStack(alignment:.leading){
                                Text(eat.eatName)
                                    .font(.headline)
                                HStack(spacing:0){
                                    Text("100 г")
                                        .foregroundColor(.green)
                                    Text(" - \(eat.eatCalories) ккал")
                                }
                            }
                            Color(.white)
                        }.onTapGesture {
                            print("меняем граммы")
                        }
                        if !self.chooseEat.contains(eat.eatName){
                            BaseImageView(imageName: "circle")
                                .frame(width: 30, height: 30)
                                .onTapGesture {
                                    self.data.append(EatModel(eatName: eat.eatName, eatCalories: 10))
                                    self.chooseEat.append(eat.eatName)
                                }
                        }else{
                            BaseImageView(imageName: "checked")
                                .frame(width: 30, height: 30)
                                .onTapGesture {
                                    self.chooseEat.removeAll { $0 == eat.eatName }
                                    self.data.removeAll { $0.eatName == eat.eatName }
                                }
                        }
                    }
                }
            }
            CustomTapBarView().environmentObject(openView)
        }
        .edgesIgnoringSafeArea(.bottom)
    }
}

extension AddEatView{
    func eatChoose() {
        switch self.eatTime.eatingTime{
        case .breakfast:
            self.eatData.breakfast.append(contentsOf: self.data)
        case .lunch:
            self.eatData.lunch.append(contentsOf: self.data)
        case .dinner:
            self.eatData.dinner.append(contentsOf: self.data)
        case .snack:
            self.eatData.snack.append(contentsOf: self.data)
        }
    }
}

struct AddEatView_Previews: PreviewProvider {
    static var previews: some View {
        AddEatView().environmentObject(NavigationViewModel()).environmentObject(EatDataViewModel())
    }
}
