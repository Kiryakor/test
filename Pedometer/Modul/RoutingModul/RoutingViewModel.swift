//
//  RoutingViewModel.swift
//  Pedometer
//
//  Created by Кирилл on 17.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import Foundation

class RoutingViewModel {
    
    func checkFirstView() -> NavigationEnum{
        let sex = UserDefaults.standard.string(forKey: userDefaultsKey.sex.rawValue) ?? ""
        let heigh = UserDefaults.standard.string(forKey: userDefaultsKey.heigh.rawValue) ?? ""
        let width = UserDefaults.standard.string(forKey: userDefaultsKey.width.rawValue) ?? ""
        
        if sex == ""{
            return .isSexChoose
        }else if heigh == "" || width == ""{
            return .isMyProfileView
        }else{
            return .isPresentMainView
        }
    }
}
