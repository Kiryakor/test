//
//  FirstStartView.swift
//  Pedometer
//
//  Created by Кирилл on 15.08.2020.
//  Copyright © 2020 Кирилл. All rights reserved.
//

import SwiftUI
import HealthKit

struct RoutingView: View {
    
    @EnvironmentObject var presentedView:NavigationViewModel
    @EnvironmentObject var eatData:EatDataViewModel
    @EnvironmentObject var eatingTime:EatingTimeViewModel
    
    var routingViewModel = RoutingViewModel()
    
    var body: some View {
        VStack{
            returnView()
        }.onAppear(){
            self.presentedView.presentedView = self.routingViewModel.checkFirstView()
        }
    }
    
    func returnView() -> AnyView{
        switch presentedView.presentedView{
            case .isSexChoose:
                return AnyView(ChooseSexView().environmentObject(presentedView))
            case .isMyProfileView:
                return AnyView(MyProfileView().environmentObject(presentedView))
            case .isPresentMainView:
                return AnyView(MainView().environmentObject(presentedView))
            case .isAchievementsView:
                return AnyView(AchievementsView().environmentObject(presentedView))
            case .isEatView:
                return AnyView(EatView()
                    .environmentObject(presentedView)
                    .environmentObject(eatData)
                    .environmentObject(eatingTime))
            case .isTrainingView:
                return AnyView(TrainingView().environmentObject(presentedView))
            case .isAddEatView:
                return AnyView(AddEatView()
                    .environmentObject(presentedView)
                    .environmentObject(eatData)
                    .environmentObject(eatingTime))
            case .isDetailEatView:
                return AnyView(DetailEatView()
                        .environmentObject(presentedView))
        }
    }
}

struct FirstStartView_Previews: PreviewProvider {
    static var previews: some View {
        RoutingView().environmentObject(NavigationViewModel())
    }
}
